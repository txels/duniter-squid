# Duniter-Squid

A [Squid](https://subsquid.io)-based indexer.
It takes [ĞDev](https://forum.duniter.org/c/currencies/gdev/63) data and serves it via GraphQL API.

## Run in production

1. Copy files from this repo to a new local directory:

   * docker-compose.yml
   * .env.example

   Rename `.env.example` to `.env` and adapt it to your own configuration.

2. Start docker container with.

  ```bash
  docker compose up -d
  ```

## Dev requirements

* node 18.x
* docker
* npm

## Run

Example commands below use [sqd](https://docs.subsquid.io/squid-cli/).
Please [install](https://docs.subsquid.io/squid-cli/installation/) it before proceeding.

```bash
# 1. Install dependencies
npm ci

# 2. Start target Postgres database and detach
sqd up

# 3. Build the project
sqd build

# 4. Start both the squid processor and the GraphQL server
sqd run .
```

A GraphiQL playground will be available at [localhost:4350/graphql](http://localhost:4350/graphql).

## Dev flow

TL;DR

```sh
# after modifying typegen.json
sqd typegen
# after modifying schema.graphql
sqd codegen
sqd migration:generate
# reboot the database
sqd down && sqd up
# build and run
sqd build && sqd run .
```

The rest was here in the squid substrate template.

### 1. Define database schema

Start development by defining the schema of the target database via `schema.graphql`.
Schema definition consists of regular graphql type declarations annotated with custom directives.
Full description of `schema.graphql` dialect is available [here](https://docs.subsquid.io/store/postgres/schema-file/).

### 2. Generate TypeORM classes

Mapping developers use [TypeORM](https://typeorm.io) entities
to interact with the target database during data processing. All necessary entity classes are
[generated](https://docs.subsquid.io/store/postgres/schema-file/intro/) by the squid framework from `schema.graphql`. This is done by running `npx squid-typeorm-codegen`
or (equivalently) `sqd codegen` command.

### 3. Generate database migration

All database changes are applied through migration files located at `db/migrations`.
`squid-typeorm-migration(1)` tool provides several commands to drive the process.
It is all [TypeORM](https://typeorm.io/#/migrations) under the hood.

```bash
# Connect to database, analyze its state and generate migration to match the target schema.
# The target schema is derived from entity classes generated earlier.
# Don't forget to compile your entity classes beforehand!
npx squid-typeorm-migration generate

# Create template file for custom database changes
npx squid-typeorm-migration create

# Apply database migrations from `db/migrations`
npx squid-typeorm-migration apply

# Revert the last performed migration
npx squid-typeorm-migration revert         
```

Available `sqd` shortcuts:

```bash
# Build the project, remove any old migrations, then run `npx squid-typeorm-migration generate`
sqd migration:generate

# Run npx squid-typeorm-migration apply
sqd migration:apply
```

### 4. Generate TypeScript definitions for substrate events, calls and storage

This is an optional part, but it is very advisable.

Event, call and runtime storage data come to mapping handlers as raw untyped json.
While it is possible to work with raw untyped json data,
it's extremely error-prone and the json structure may change over time due to runtime upgrades.

Squid framework provides a tool for generating type-safe wrappers around events, calls and runtime storage items for
each historical change in the spec version. See the [Substrate typegen](https://docs.subsquid.io/substrate-indexing/squid-substrate-typegen/) documentation page.

## Deploy the Squid

See <https://duniter.org/wiki/duniter-v2/indexers/duniter-squid/>

## TODO

* [x] Refac way to process data from events (new, fetch needed...)
* [ ] Add events:
  * [x] identity.IdtyCreated
  * [x] identity.IdtyConfirmed
  * [ ] identity.IdtyValidated ?
  * [x] identity.IdtyChangedOwnerKey
  * [ ] identity.IdtyRemoved
  * [ ] membership.MembershipAcquired
  * [ ] membership.MembershipExpired
  * [ ] membership.MembershipRenewed
  * [ ] membership.MembershipRequested
  * [ ] membership.MembershipRevoked
  * [ ] membership.PendingMembershipExpired
  * [ ] smithMembership.MembershipAcquired
  * [ ] smithMembership.MembershipExpired
  * [ ] smithMembership.MembershipRenewed
  * [ ] smithMembership.MembershipRequested
  * [ ] smithMembership.MembershipRevoked
  * [ ] smithMembership.PendingMembershipExpired
  * [x] account.AccountLinked
  * [x] account.AccountUnlinked
  * [x] cert.NewCert TODO if the cert is already existing, what to do?
  * [x] cert.RenewedCert
  * [x] cert.RemovedCert
  * [x] smithCert.NewCert
  * [x] smithCert.RenewedCert
  * [x] smithCert.RemovedCert

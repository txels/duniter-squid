import { TypeormDatabaseWithCache } from "@belopash/typeorm-store";
import { Block, processor } from "./processor";
import { events as events_t, constants } from "./types";
import { Ctx, NewData } from "./types_custom";
import { ss58encode } from "./utils";
import { saveGenesis } from "./genesis";
import { saveBlock, saveCall, saveEvent, saveExtrinsic } from "./giant-squid";
import assert from "assert";
import { DataHandler } from "./data_handler";

// main processor loop able to manage a batch of blocks
processor.run(new TypeormDatabaseWithCache(), async (ctx) => {
  // this part is adapted from giant squid
  // https://github.com/subsquid-labs/giant-squid-explorer/blob/main/src/main.ts
  for (const { header, calls, events, extrinsics } of ctx.blocks) {
    ctx.log.debug(
      `block ${header.height}: extrinsics - ${extrinsics.length}, calls - ${calls.length}, events - ${events.length}`
    );
    // save the block header
    const genesisBlock = await saveBlock(ctx, header);
    // manage genesis state
    if (header.height == 0) {
      await saveGenesis(ctx, genesisBlock);
    }
    // save all extrinsics of the block
    for (const extrinsic of extrinsics) {
      await saveExtrinsic(ctx, extrinsic);
    }
    // save all calls of the block
    for (const call of calls.reverse()) {
      await saveCall(ctx, call);
    }
    // save all events of the block
    for (const event of events) {
      await saveEvent(ctx, event);
    }
  }

  const newData: NewData = {
    accounts: [],
    identitiesCreated: [],
    identitiesConfirmed: [],
    identitiesValidated: [],
    identitiesRemoved: [],
    identitiesRevoked: [],
    idtyChangedOwnerKey: [],
    membershipAdded: [],
    membershipRemoved: [],
    membershipRenewed: [],
    transfers: [],
    certCreation: [],
    certRenewal: [],
    certRemoval: [],
    accountLink: [],
    accountUnlink: [],
    smithCertAdded: [],
    smithCertRemoved: [],
    smithExcluded: [],
    smithPromoted: [],
    smithInvited: [],
    smithAccepted: [],
  };
  collectDataFromEvents(ctx, newData);

  const dataHandler = new DataHandler();
  await dataHandler.processNewData(newData, ctx);

  // HACK to handle circular dependency, we are forced to create accounts+ identities
  // first and commit the changes before updating the rest
  await dataHandler.handleNewAccountsApart(ctx, newData);
  await dataHandler.handleNewIdentitiesApart(ctx, newData);

  // store data
  await dataHandler.storeData(ctx);
});

function collectDataFromEvents(ctx: Ctx, newData: NewData) {
  const silence_events = [
    events_t.system.extrinsicSuccess.name,
    events_t.system.killedAccount.name,
    events_t.certification.certRemoved.name,
    events_t.session.newSession.name,
    events_t.imOnline.allGood.name,
    events_t.membership.membershipRemoved.name,
    events_t.universalDividend.udsAutoPaid.name,
    events_t.universalDividend.newUdCreated.name,
    events_t.balances.withdraw.name,
    events_t.balances.deposit.name,
    events_t.transactionPayment.transactionFeePaid.name,
    events_t.identity.idtyRemoved.name,
    events_t.quota.refunded.name,
  ];

  ctx.blocks.forEach((block) => {
    block.events.forEach((event) => {
      if (!silence_events.includes(event.name)) {
        ctx.log.info("" + block.header.height + " " + event.name);
      }
      switch (event.name) {
        case events_t.system.newAccount.name: {
          const newAccount = events_t.system.newAccount.v800.decode(event);
          newData.accounts.push(ss58encode(newAccount.account));
          break;
        }

        case events_t.account.accountLinked.name: {
          const accountLinked =
            events_t.account.accountLinked.v800.decode(event);
          newData.accountLink.push({
            accountId: ss58encode(accountLinked.who),
            index: accountLinked.identity,
          });
          break;
        }

        case events_t.account.accountUnlinked.name: {
          const accountUnlinked =
            events_t.account.accountUnlinked.v800.decode(event);
          newData.accountUnlink.push({
            accountId: ss58encode(accountUnlinked),
          });
          break;
        }

        case events_t.balances.transfer.name: {
          const transfer = events_t.balances.transfer.v800.decode(event);
          assert(
            block.header.timestamp,
            `Got an undefined timestamp at block ${block.header.height}`
          );
          newData.transfers.push({
            id: event.id,
            blockNumber: block.header.height,
            timestamp: new Date(block.header.timestamp),
            from: ss58encode(transfer.from),
            to: ss58encode(transfer.to),
            amount: transfer.amount,
          });
          break;
        }

        case events_t.identity.idtyCreated.name: {
          const idtyCreated = events_t.identity.idtyCreated.v800.decode(event);
          newData.identitiesCreated.push({
            id: event.id,
            index: idtyCreated.idtyIndex,
            accountId: ss58encode(idtyCreated.ownerKey),
            blockNumber: block.header.height,
            expireOn:
              block.header.height +
              constants.identity.idtyCreationPeriod.v800.get(event.block),
            event: event,
          });
          break;
        }

        case events_t.identity.idtyConfirmed.name: {
          const idtyConfirmed =
            events_t.identity.idtyConfirmed.v800.decode(event);
          newData.identitiesConfirmed.push({
            id: event.id,
            index: idtyConfirmed.idtyIndex,
            name: idtyConfirmed.name,
            blockNumber: block.header.height,
            expireOn:
              block.header.height +
              constants.identity.confirmPeriod.v800.get(event.block),
          });
          break;
        }

        case events_t.identity.idtyValidated.name: {
          const idtyValidated =
            events_t.identity.idtyValidated.v800.decode(event);
          newData.identitiesValidated.push({
            id: event.id,
            index: idtyValidated.idtyIndex,
            blockNumber: block.header.height,
            expireOn:
              block.header.height +
              constants.identity.validationPeriod.v800.get(event.block),
          });
          break;
        }

        case events_t.identity.idtyRemoved.name: {
          const idtyRemoved = events_t.identity.idtyRemoved.v800.decode(event);
          newData.identitiesRemoved.push({
            id: event.id,
            index: idtyRemoved.idtyIndex,
            reason: idtyRemoved.reason,
            blockNumber: block.header.height,
            expireOn:
              block.header.height +
              constants.identity.deletionPeriod.v800.get(event.block),
          });
          break;
        }

        case events_t.identity.idtyRevoked.name: {
          const idtyRevoked = events_t.identity.idtyRevoked.v800.decode(event);
          newData.identitiesRevoked.push({
            id: event.id,
            index: idtyRevoked.idtyIndex,
            reason: idtyRevoked.reason,
            blockNumber: block.header.height,
            expireOn:
              block.header.height +
              constants.identity.autorevocationPeriod.v800.get(event.block),
          });
          break;
        }

        case events_t.identity.idtyChangedOwnerKey.name: {
          const idtyChangedOwnerKey =
            events_t.identity.idtyChangedOwnerKey.v800.decode(event);
          newData.idtyChangedOwnerKey.push({
            id: event.id,
            index: idtyChangedOwnerKey.idtyIndex,
            accountId: ss58encode(idtyChangedOwnerKey.newOwnerKey),
            blockNumber: block.header.height,
            expireOn:
              block.header.height +
              constants.identity.changeOwnerKeyPeriod.v800.get(event.block),
          });
          break;
        }

        case events_t.membership.membershipAdded.name: {
          const membershipAdded =
            events_t.membership.membershipAdded.v800.decode(event);
          newData.membershipAdded.push({
            id: event.id,
            index: membershipAdded.member,
            expire_on: membershipAdded.expireOn,
            event: event,
          });
          break;
        }

        case events_t.membership.membershipRemoved.name: {
          const membershipRemoved =
            events_t.membership.membershipRemoved.v800.decode(event);
          newData.membershipRemoved.push({
            id: event.id,
            index: membershipRemoved.member,
            reason: membershipRemoved.reason,
            event: event,
          });
          break;
        }

        case events_t.membership.membershipRenewed.name: {
          const membershipRenewed =
            events_t.membership.membershipRenewed.v800.decode(event);
          newData.membershipRenewed.push({
            id: event.id,
            index: membershipRenewed.member,
            expire_on: membershipRenewed.expireOn,
            event: event,
          });
          break;
        }

        case events_t.certification.certAdded.name: {
          const certAdded = events_t.certification.certAdded.v800.decode(event);
          newData.certCreation.push({
            id: event.id,
            issuerId: certAdded.issuer,
            receiverId: certAdded.receiver,
            createdOn: block.header.height,
            expireOn:
              block.header.height +
              constants.certification.validityPeriod.v800.get(event.block),
            event: event,
          });
          break;
        }

        case events_t.certification.certRenewed.name: {
          const certRenewed =
            events_t.certification.certRenewed.v800.decode(event);
          newData.certRenewal.push({
            id: event.id,
            issuerId: certRenewed.issuer,
            receiverId: certRenewed.receiver,
            blockNumber: block.header.height,
            expireOn:
              block.header.height +
              constants.certification.validityPeriod.v800.get(event.block),
            event: event,
          });
          break;
        }

        case events_t.certification.certRemoved.name: {
          const certRemoved =
            events_t.certification.certRemoved.v800.decode(event);
          newData.certRemoval.push({
            id: event.id,
            issuerId: certRemoved.issuer,
            receiverId: certRemoved.receiver,
            blockNumber: block.header.height,
            event: event,
          });
          break;
        }

        case events_t.smithMembers.smithCertAdded.name: {
          const smithCertReceived =
            events_t.smithMembers.smithCertAdded.v800.decode(event);
          newData.smithCertAdded.push({
            id: event.id,
            issuerId: smithCertReceived.issuer,
            receiverId: smithCertReceived.receiver,
            createdOn: block.header.height,
          });
          break;
        }

        case events_t.smithMembers.smithCertRemoved.name: {
          const smithCertRemoved =
            events_t.smithMembers.smithCertRemoved.v800.decode(event);
          newData.smithCertRemoved.push({
            id: event.id,
            issuerId: smithCertRemoved.issuer,
            receiverId: smithCertRemoved.receiver,
          });
          break;
        }

        case events_t.smithMembers.smithMembershipAdded.name: {
          const promotedToSmith =
            events_t.smithMembers.smithMembershipAdded.v800.decode(event);
          newData.smithPromoted.push({
            id: event.id,
            idtyIndex: promotedToSmith.idtyIndex,
          });
          break;
        }

        case events_t.smithMembers.smithMembershipRemoved.name: {
          const smithExcluded =
            events_t.smithMembers.smithMembershipRemoved.v800.decode(event);
          newData.smithExcluded.push({
            id: event.id,
            idtyIndex: smithExcluded.idtyIndex,
          });
          break;
        }

        case events_t.smithMembers.invitationSent.name: {
          const smithInvited =
            events_t.smithMembers.invitationSent.v800.decode(event);
          newData.smithInvited.push({
            id: event.id,
            idtyIndex: smithInvited.receiver,
            invitedBy: smithInvited.issuer,
          });
          break;
        }

        case events_t.smithMembers.invitationAccepted.name: {
          const smithInvitationAccepted =
            events_t.smithMembers.invitationAccepted.v800.decode(event);
          newData.smithAccepted.push({
            id: event.id,
            idtyIndex: smithInvitationAccepted.idtyIndex,
          });
          break;
        }

        default:
          // ctx.log.info(`Unhandled event ${event.name}`)
          break;
      }
    });
  });
}

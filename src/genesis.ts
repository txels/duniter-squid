import type { Address, IdtyIndex, Ctx, Genesis, TransactionHistory, Tx } from "./types_custom";
import { readFileSync } from "fs";
import { Account, Cert, SmithCert, Identity, MembershipEvent, Transfer, ChangeOwnerKey, SmithStatus, Block, Event, EventType, IdentityStatus } from "./model";
import path from "path/posix";
import { bytes } from "./model/generated/marshal";
import { bytesToString } from "./utils";

export async function saveGenesis(ctx: Ctx, block: Block) {
  const genesis_path = `./assets/${process.env.GENESIS_FILE}`;
  const history_path = `./assets/${process.env.HISTORY_FILE}`;

  ctx.log.info("Loading genesis");

  // Read genesis json
  const genesis: Genesis = JSON.parse(readFileSync(path.resolve(process.cwd(), genesis_path)).toString()).genesis.runtimeAndCode.runtime;

  const accounts: Map<Address, Account> = new Map();
  const identities: Map<IdtyIndex, Identity> = new Map();
  const chok: ChangeOwnerKey[] = [];
  const certs: Cert[] = [];
  const smithCerts: SmithCert[] = [];
  const membershipsEvents: MembershipEvent[] = [];

  // create fake genesis event to get block height and timestamp
  const genesis_event = new Event({
    id: "genesis-event",
    index: 0,
    block,
    phase: 'genesis-phase',
    pallet: 'genesis-pallet',
    name: 'genesis-name',
  });
  await ctx.store.insert(genesis_event);

  // collect accounts
  for (const [address] of Object.entries(genesis.account.accounts)) {
    accounts.set(
      address,
      new Account({
        id: address,
      })
    );
  }

  // collect identities
  for (const idty of genesis.identity.identities) {
    const account = accounts.get(idty.value.owner_key);
    const the_identity = new Identity({
      id: `genesis-identity-${idty.index}`,
      index: idty.index,
      account,
      name: bytesToString(idty.name),
      status: idty.value.status,
      isMember: false,
      lastChangeOn: 0,
      createdOn: 0,
      createdIn: genesis_event,
      expireOn: 0,
    });

    // add identity to list
    identities.set(idty.index, the_identity);

    // if changed owner key, also add event
    if (idty.value.old_owner_key != null) {
      const old_account_id = idty.value.old_owner_key[0];
      let old_account = accounts.get(old_account_id);
      if (old_account == null) {
        // this can happen if the old account is emptied from its content
        old_account = new Account({ id: old_account_id });
        accounts.set(old_account_id, old_account);
      }
      chok.push(
        new ChangeOwnerKey({
          id: `genesis-changeownerkey-${idty.index}`,
          identity: the_identity,
          previous: old_account,
          next: account,
          blockNumber: 0,
        })
      );
    }
  }

  // collect memberships
  for (const [idtyIndex, mshipInfo] of Object.entries(genesis.membership.memberships)) {
    const identity = identities.get(parseInt(idtyIndex))!;
    identity.isMember = true;
    identity.expireOn = mshipInfo.expire_on;
    membershipsEvents.push(
      new MembershipEvent({
        id: `genesis-membership-${idtyIndex}`,
        identity: identity,
        eventType: EventType.CREATION,
        event: genesis_event,
        blockNumber: 0,
      })
    );
  }

  // collect certifications
  for (const [receiver_index, certs_received] of Object.entries(genesis.certification.certsByReceiver)) {
    for (const [issuer_index, expiration_block] of Object.entries(certs_received)) {
      certs.push(
        new Cert({
          // cert id is different in genesis than in the rest of blockchain
          id: `genesis-cert-${issuer_index}-${receiver_index}`,
          isActive: true,
          issuer: identities.get(parseInt(issuer_index)),
          receiver: identities.get(parseInt(receiver_index)),
          createdOn: 0,
          expireOn: expiration_block as number,
        })
      );
    }
  }

  // collect smith memberships
  for (const [idtyIdex, smithCertsData] of Object.entries(genesis.smithMembers.initialSmiths)) {
    const [isOnline, certs] = smithCertsData as [boolean, number[]];
    const identity = identities.get(parseInt(idtyIdex))!;
    identity.smithStatus = SmithStatus.Smith;
    identities.set(identity.index, identity);

    for (const issuer_index of certs) {
      smithCerts.push(
        new SmithCert({
          id: `genesis-smith-membership-${issuer_index}-${idtyIdex}`,
          issuer: identities.get(issuer_index),
          receiver: identities.get(parseInt(idtyIdex)),
          createdOn: 0,
        })
      );
    }
  }

  ctx.log.info("Saving genesis");

  // insert everything in storage
  await ctx.store.insert([...accounts.values()]);
  await ctx.store.insert([...identities.values()]);
  await ctx.store.insert(certs);
  await ctx.store.insert(chok);
  await ctx.store.insert(smithCerts);
  await ctx.store.insert(membershipsEvents);
  // await ctx.store.flush(); // do not flush otherwise we lose cache

  ctx.log.info("Genesis saved");

  // ===========================================

  // accounts present in transaction history but not in genesis
  const other_accounts: Map<Address, Account> = new Map();
  const genesis_transfers: Transfer[] = [];

  ctx.log.info("Loading transaction history");

  // Read transaction history json
  const tx_history: TransactionHistory = new Map<Address, Array<Tx>>(
    Object.entries(JSON.parse(readFileSync(path.resolve(process.cwd(), history_path)).toString()))
  );

  // set of all wallets address
  const all_wallets = new Set<Address>();
  for (const [receiver, txs] of tx_history.entries()) {
    all_wallets.add(receiver);
    for (const tx of txs) {
      all_wallets.add(tx.issuer);
    }
  }

  // filter walconst that are not present in genesis and create accounts for them
  // apparently Set difference is experimental
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/difference
  // const unknown_wallets = wallets.difference(genesis_wallets)
  const unknown_wallets = new Set([...all_wallets].filter((x) => !accounts.has(x)));
  ctx.log.info(`There are ${accounts.size} genesis wallets, ${all_wallets.size} total wallets, ${unknown_wallets.size} unknown wallets`);
  // create accounts for unknown wallets
  for (const address of unknown_wallets) {
    const account = new Account({
      id: address,
    });
    accounts.set(address, account);
    other_accounts.set(address, account);
  }

  // add txs
  let genesis_tx_counter = 0;
  for (const [receiver, txs] of tx_history.entries()) {
    for (const tx of txs) {
      genesis_tx_counter += 1;
      // FIXME skip transaction with null written_time that should not have been included
      if (tx.written_time != null) {
        const date = new Date(tx.written_time * 1000); // seconds to milliseconds
        const from = accounts.get(tx.issuer);
        const to = accounts.get(receiver);
        genesis_transfers.push(
          new Transfer({
            id: `genesis_tx-${genesis_tx_counter}`,
            blockNumber: 0,
            timestamp: date,
            from,
            to,
            amount: BigInt(tx.amount),
            comment: tx.comment,
          })
        );
      }
    }
  }

  ctx.log.info("Saving transaction history");
  await ctx.store.insert([...other_accounts.values()]);
  await ctx.store.insert(genesis_transfers);
  await ctx.store.flush();
  ctx.log.info("Saved transaction history");
  ctx.log.info("======================");
  ctx.log.info("Starting blockchain indexing");
}

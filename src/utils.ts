import { Address } from "./types_custom";
import * as ss58 from "@subsquid/ss58";

// define ss58 encoding with custom prefix
const SS58_PREFIX = 42;

export function ss58encode(hex_encoded_address: string): Address {
  return ss58.codec(SS58_PREFIX).encode(hex_encoded_address);
}
export function hexToString(hex: string): string {
  return Buffer.from(hex.slice(2), "hex").toString("utf8");
}

export function bytesToString(bytes: Array<number>): string {
  return bytes.map(code => String.fromCharCode(code)).join('');
}

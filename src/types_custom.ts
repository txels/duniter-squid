import {
  Account,
  Cert,
  CertEvent,
  ChangeOwnerKey,
  Identity,
  IdentityStatus,
  MembershipEvent,
  SmithCert,
  Transfer,
} from "./model";
import { ProcessorContext, Event } from "./processor";
import { StoreWithCache } from "@belopash/typeorm-store";
import { MembershipRemovalReason, RemovalReason, RevocationReason } from "./types/v800";

// type aliases
type BlockNumber = number;
export type Address = string;
export type IdtyIndex = number;
export type Ctx = ProcessorContext<StoreWithCache>;

// =========================== Genesis =========================== //

// define genesis interfaces
// duniter build-spec --chain gdev_dev 1> ./specs.json
// pallet / genesis config
export interface Genesis {
  system: any;
  account: GenAccounts;
  babe: any;
  parameters: any;
  balances: any;
  authorityMembers: GenInitialAuthorities;
  session: any;
  grandpa: any;
  imOnline: any;
  authorityDiscovery: any;
  sudo: any;
  technicalCommittee: any;
  universalDividend: any;
  identity: GenIdentities;
  membership: GenMemberships;
  certification: GenCerts;
  smithMembers: GenSmithMembers;
  treasury: any;
}
export type TransactionHistory = Map<Address, Array<Tx>>;
export interface Tx {
  issuer: Address;
  amount: string;
  written_time: number;
  comment: string;
}

interface GenIdentities {
  identities: Array<GenIdentity>;
}

interface GenIdentity {
  index: number;
  name: Array<number>;
  value: GenIdtyValue;
}

interface GenIdtyValue {
  data: GenIdtyData;
  next_creatable_identity_on: number;
  old_owner_key: string | null;
  owner_key: string;
  removable_on: number;
  status: IdentityStatus;
}

interface GenIdtyData {
  first_eligible_ud: number;
}

interface GenMemberships {
  memberships: Map<IdtyIndex, GenMembership>;
}

interface GenMembership {
  expire_on: number;
}

interface GenSmithMembers {
  initialSmiths: Map<string, [boolean, number[]]>;
}

interface GenAccounts {
  accounts: Map<string, GenAccount>;
  treasuryBalance: number;
}

interface GenAccount {
  random_id: string;
  balance: number;
  is_identity: boolean;
}

interface GenCerts {
  applyCertPeriodAtGenesis: boolean;
  certsByReceiver: Map<number, Map<number, number>>;
}

interface GenInitialAuthorities {
  initialAuthorities: Map<number, Array<any>>;
}

// =========================== DataHandler =========================== //

// a way to group data prepared for database insertion
export interface Data {
  accounts: Map<Address, Account>;
  identities: Map<IdtyIndex, Identity>;
  membershipEvents: MembershipEvent[];
  changeOwnerKey: ChangeOwnerKey[];
  transfers: Transfer[];
  certification: Map<[IdtyIndex, IdtyIndex], Cert>;
  certEvent: CertEvent[];
  smithCert: Map<[IdtyIndex, IdtyIndex], SmithCert>;
}

// =========================== Events =========================== //

// a way to group data returned from events
// this contains partial data to be turned into types
export interface NewData {
  accounts: Address[];
  identitiesCreated: IdtyCreatedEvent[];
  identitiesConfirmed: IdtyConfirmedEvent[];
  identitiesValidated: IdtyValidatedEvent[];
  identitiesRemoved: IdtyRemovedEvent[];
  identitiesRevoked: IdtyRevokedEvent[];
  idtyChangedOwnerKey: IdtyChangedOwnerKeyEvent[];
  membershipAdded: MembershipAddedEvent[];
  membershipRemoved: MembershipRemovedEvent[];
  membershipRenewed: MembershipRenewedEvent[];
  transfers: TransferEvent[];
  certCreation: CertCreationEvent[];
  certRenewal: CertRenewalEvent[];
  certRemoval: CertRemovalEvent[];
  accountLink: AccountLinkEvent[];
  accountUnlink: AccountUnlinkEvent[];
  smithCertAdded: SmithCertAddedEvent[];
  smithCertRemoved: SmithCertRemovedEvent[],
  smithPromoted: SmithPromotedEvent[];
  smithExcluded: SmithExcludedEvent[];
  smithInvited: SmithInvitedEvent[];
  smithAccepted: SmithAcceptedEvent[];
}

// id is always the id of the creation event
interface TransferEvent {
  id: string;
  blockNumber: BlockNumber;
  timestamp: Date;
  from: Address;
  to: Address;
  amount: bigint;
}

interface CertCreationEvent {
  id: string;
  issuerId: IdtyIndex;
  receiverId: IdtyIndex;
  createdOn: BlockNumber;
  expireOn: BlockNumber;
  event: Event;
}

interface CertRenewalEvent {
  id: string;
  issuerId: IdtyIndex;
  receiverId: IdtyIndex;
  blockNumber: BlockNumber;
  expireOn: BlockNumber;
  event: Event;
}

interface CertRemovalEvent {
  id: string;
  issuerId: IdtyIndex;
  receiverId: IdtyIndex;
  blockNumber: BlockNumber;
  event: Event;
}

interface SmithPromotedEvent {
  id: string;
  idtyIndex: IdtyIndex;
}

interface SmithCertAddedEvent {
  id: string;
  issuerId: IdtyIndex;
  receiverId: IdtyIndex;
  createdOn: BlockNumber;
}

interface SmithCertRemovedEvent {
  id: string;
  issuerId: IdtyIndex;
  receiverId: IdtyIndex;
}

interface SmithExcludedEvent {
  id: string;
  idtyIndex: IdtyIndex;
}

interface SmithInvitedEvent {
  id: string;
  idtyIndex: IdtyIndex;
  invitedBy: IdtyIndex;
}

interface SmithAcceptedEvent {
  id: string;
  idtyIndex: IdtyIndex;
}

interface IdtyCreatedEvent {
  id: string;
  index: IdtyIndex;
  accountId: Address;
  blockNumber: BlockNumber;
  expireOn: BlockNumber;
  event: Event;
}

interface IdtyConfirmedEvent {
  id: string;
  index: IdtyIndex;
  name: string;
  blockNumber: BlockNumber;
  expireOn: BlockNumber;
}

interface IdtyValidatedEvent {
  id: string;
  index: IdtyIndex;
  blockNumber: BlockNumber;
  expireOn: BlockNumber;
}

interface IdtyRemovedEvent {
  id: string;
  index: IdtyIndex;
  reason: RemovalReason;
  blockNumber: BlockNumber;
  expireOn: BlockNumber;
}

interface IdtyRevokedEvent {
  id: string;
  index: IdtyIndex;
  reason: RevocationReason;
  blockNumber: BlockNumber;
  expireOn: BlockNumber;
}

interface IdtyChangedOwnerKeyEvent {
  id: string;
  index: IdtyIndex;
  accountId: Address;
  blockNumber: BlockNumber;
  expireOn: BlockNumber;
}

interface MembershipAddedEvent {
  id: string;
  index: IdtyIndex;
  expire_on: number;
  event: Event;
}

interface MembershipRemovedEvent {
  id: string;
  index: IdtyIndex;
  reason: MembershipRemovalReason;
  event: Event;
}

interface MembershipRenewedEvent {
  id: string;
  index: IdtyIndex;
  expire_on: number;
  event: Event;
}

interface AccountLinkEvent {
  accountId: Address;
  index: IdtyIndex;
}

interface AccountUnlinkEvent {
  accountId: Address;
}

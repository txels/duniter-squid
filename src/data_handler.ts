import {
  Account,
  Cert,
  CertEvent,
  SmithCert,
  ChangeOwnerKey,
  Identity,
  Transfer,
  IdentityStatus,
  SmithStatus,
  MembershipEvent,
  Block,
  EventType,
  Event,
} from "./model";
import { In } from "typeorm";
import { Address, Ctx, Data, IdtyIndex, NewData } from "./types_custom";
import assert from "assert";
import { hexToString } from "./utils";

export class DataHandler {
  private data: Data;

  constructor() {
    this.data = {
      accounts: new Map(),
      identities: new Map(),
      membershipEvents: [],
      changeOwnerKey: [],
      transfers: [],
      certification: new Map(),
      certEvent: [],
      smithCert: new Map(),
    };
  }

  async processNewData(newData: NewData, ctx: Ctx) {
    // Process accounts
    for (const accountId of newData.accounts) {
      const newAccount = new Account({ id: accountId });
      this.data.accounts.set(accountId, newAccount);
      ctx.log.info(`Added account ${accountId}`);
    }

    // Process transfers
    for (const transfer of newData.transfers) {
      //   const fromAccount = await this.getOrCreateAccount(ctx, transfer.from);
      //   const toAccount = await this.getOrCreateAccount(ctx, transfer.to);
      ctx.log.info(
        `New transaction: ${transfer.from} to ${transfer.to} of ${transfer.amount} tokens`
      );
      // should never fail because source of transfer must be an existing account
      const fromAccount = await this.getAccountByAddressOrFail(ctx, transfer.from);
      // shoud never fail because destination of transfer must be existing account or raise System.NewAccount
      const toAccount = await this.getAccountByAddressOrFail(ctx, transfer.to);
      const newTransfer = new Transfer({
        id: transfer.id,
        blockNumber: transfer.blockNumber,
        timestamp: transfer.timestamp,
        from: fromAccount,
        to: toAccount,
        amount: transfer.amount,
      });
      this.data.transfers.push(newTransfer);
    }

    // Process identities created
    for (const identity of newData.identitiesCreated) {
      const account = await this.getOrCreateAccount(ctx, identity.accountId);
      const newIdentity = new Identity({
        id: identity.id,
        index: identity.index,
        name: identity.id, // Using the id of the creation event as the name for unconfirmed identities
        status: IdentityStatus.Unconfirmed,
        account,
        isMember: false,
        lastChangeOn: identity.blockNumber,
        createdOn: identity.blockNumber,
        createdIn: await ctx.store.getOrFail(Event, identity.event.id),
        expireOn: identity.expireOn,
      });
      this.data.identities.set(identity.index, newIdentity);
    }

    // Process identities confirmed
    for (const identity of newData.identitiesConfirmed) {
      const idty = await this.getIdtyByIndexOrFail(ctx, identity.index);
      idty.name = hexToString(identity.name);
      idty.status = IdentityStatus.Unvalidated;
      idty.lastChangeOn = identity.blockNumber;
      idty.expireOn = identity.expireOn;
      this.data.identities.set(identity.index, idty);
    }

    // Process identities validated
    for (const identity of newData.identitiesValidated) {
      const idty = await this.getIdtyByIndexOrFail(ctx, identity.index);
      idty.status = IdentityStatus.Member;
      idty.lastChangeOn = identity.blockNumber;
      idty.expireOn = identity.expireOn;
      this.data.identities.set(identity.index, idty);
    }

    // Process identities removed
    for (const identity of newData.identitiesRemoved) {
      const idty = await this.getIdtyByIndexOrFail(ctx, identity.index);
      ctx.log.info(
        `Set identity ${identity.index} status to Removed for reason ${identity.reason.__kind}`
      );
      idty.status = IdentityStatus.Removed;
      idty.lastChangeOn = identity.blockNumber;
      idty.expireOn = identity.expireOn;
      this.data.identities.set(identity.index, idty);
    }

    // Process identities revoked
    for (const identity of newData.identitiesRevoked) {
      const idty = await this.getIdtyByIndexOrFail(ctx, identity.index);
      ctx.log.info(
        `Set identity ${identity.index} status to Revoked for reason ${identity.reason.__kind}`
      );
      idty.status = IdentityStatus.Revoked;
      idty.lastChangeOn = identity.blockNumber;
      idty.expireOn = identity.expireOn;
      this.data.identities.set(identity.index, idty);
    }

    // Process identity owner key changes
    for (const idtyChange of newData.idtyChangedOwnerKey) {
      const idty = await this.getIdtyByIndexOrFail(ctx, idtyChange.index);

      const new_account = await this.getOrCreateAccount(ctx, idtyChange.accountId);
      // add change owner key event
      this.data.changeOwnerKey.push(
        new ChangeOwnerKey({
          id: idtyChange.id,
          identity: idty,
          previous: idty.account,
          next: new_account,
          blockNumber: idtyChange.blockNumber,
        })
      );
      // after adding the event we can update the identity
      idty.account = new_account;
      idty.lastChangeOn = idtyChange.blockNumber;
      idty.expireOn = idtyChange.expireOn;
      this.data.identities.set(idtyChange.index, idty);
    }

    // Process membership added
    for (const membershipAdded of newData.membershipAdded) {
      const { id, index, expire_on, event } = membershipAdded;

      const identity = await this.getIdtyByIndexOrFail(ctx, index);
      identity.status = IdentityStatus.Member
      identity.expireOn = expire_on;
      identity.isMember = true;
      identity.lastChangeOn = event.block.height;

      // Create membership
      this.data.membershipEvents.push(
        new MembershipEvent({
          id: `membership-creation-${index}-${id}`,
          identity,
          eventType: EventType.CREATION,
          event: await ctx.store.getOrFail(Event, event.id),
          blockNumber: event.block.height,
        })
      );

      this.data.identities.set(identity.index, identity);
    }

    // Process membership renewed
    for (const membershipRenewed of newData.membershipRenewed) {
      const { id, index, expire_on, event } = membershipRenewed;
      const identity = await this.getIdtyByIndexOrFail(ctx, index);

      identity.status = IdentityStatus.Member
      identity.expireOn = expire_on;
      identity.isMember = true;
      identity.lastChangeOn = event.block.height;

      // Create membership
      this.data.membershipEvents.push(
        new MembershipEvent({
          id: `membership-renewal-${index}-${id}`,
          identity,
          eventType: EventType.RENEWAL,
          event: await ctx.store.getOrFail(Event, event.id),
          blockNumber: event.block.height,
        })
      );

      this.data.identities.set(identity.index, identity);
    }

    // Process membership removed
    for (const membershipRemoved of newData.membershipRemoved) {
      const { id, index, event } = membershipRemoved;
      const identity = await this.getIdtyByIndexOrFail(ctx, index);
      identity.status = IdentityStatus.Removed;
      identity.isMember = false;
      identity.lastChangeOn = event.block.height;

      this.data.membershipEvents.push(
        new MembershipEvent({
          id: `membership-removal-${index}-${id}`,
          identity,
          eventType: EventType.REMOVAL,
          event: await ctx.store.getOrFail(Event, event.id),
          blockNumber: event.block.height,
        })
      );
      this.data.identities.set(identity.index, identity);
    }

    // Process certifications creations
    for (const c of newData.certCreation) {
      const { id, issuerId, receiverId, createdOn, expireOn, event } = c;
      // first creation of the cert
      let cert = await ctx.store.findOne(Cert, {
        relations: { issuer: true, receiver: true },
        where: { issuer: { index: issuerId }, receiver: { index: receiverId } },
      });

      if (cert == null) {
        const issuer = await this.getIdtyByIndexOrFail(ctx, issuerId);
        const receiver = await this.getIdtyByIndexOrFail(ctx, receiverId);
        cert = new Cert({
          id,
          isActive: true,
          issuer,
          receiver,
          createdOn,
          expireOn,
        });
        // the cert has already existed, expired, and is created again
        // we update it accordingly
      } else {
        cert.isActive = true;
        cert.createdOn = createdOn;
        cert.expireOn = expireOn;
      }

      // update cert and add event
      this.data.certification.set([issuerId, receiverId], cert);
      this.data.certEvent.push(
        new CertEvent({
          id,
          cert,
          blockNumber: createdOn,
          eventType: EventType.CREATION,
          event: await ctx.store.getOrFail(Event, event.id),
        })
      );
    }

    // Process certifications renewals
    for (const c of newData.certRenewal) {
      const { id, issuerId, receiverId, blockNumber, expireOn, event } = c;
      // should never fail because cert renewal can only happen on existing cert
      // and can not be renewed at the same block as created (delay)
      const cert = await ctx.store.findOneOrFail(Cert, {
        relations: { issuer: true, receiver: true },
        where: { issuer: { index: issuerId }, receiver: { index: receiverId } },
      });
      // update expiration date
      cert.expireOn = expireOn;
      this.data.certification.set([issuerId, receiverId], cert);
      this.data.certEvent.push(
        new CertEvent({
          id,
          cert,
          blockNumber,
          eventType: EventType.RENEWAL,
          event: await ctx.store.getOrFail(Event, event.id),
        })
      );
    }

    // Process certifications removals
    for (const c of newData.certRemoval) {
      const { id, issuerId, receiverId, blockNumber, event } = c;
      // should never fail because cert removal can only happen on existing cert
      // and cert should not be removed at their creation block
      const cert = await ctx.store.findOneOrFail(Cert, {
        relations: { issuer: true, receiver: true },
        where: { issuer: { index: issuerId }, receiver: { index: receiverId } },
      });
      // update cert
      cert.isActive = false;
      cert.expireOn = blockNumber;
      this.data.certification.set([issuerId, receiverId], cert);

      this.data.certEvent.push(
        new CertEvent({
          id,
          cert,
          blockNumber,
          eventType: EventType.REMOVAL,
          event: await ctx.store.getOrFail(Event, event.id),
        })
      );
    }

    // Process Smith certifications
    for (const c of newData.smithCertAdded) {
      const { id, issuerId, receiverId, createdOn } = c;
      let cert = await ctx.store.findOne(SmithCert, {
        relations: { issuer: true, receiver: true },
        where: { issuer: { index: issuerId }, receiver: { index: receiverId } },
      });
      if (cert == null) {
        const issuer = await this.getIdtyByIndexOrFail(ctx, issuerId);
        const receiver = await this.getIdtyByIndexOrFail(ctx, receiverId);
        cert = new SmithCert({
          id,
          issuer,
          receiver,
          createdOn,
        });
      } else {
        cert.createdOn = createdOn;
      }
      this.data.smithCert.set([issuerId, receiverId], cert);
    }

    // Process remove smith cert
    for (const smithCertRemoved of newData.smithCertRemoved) {
      const { issuerId, receiverId } = smithCertRemoved;

      this.data.smithCert.delete([issuerId, receiverId]);
    }

    // Process Smith invitation sent
    for (const invitedSmith of newData.smithInvited) {
      const { idtyIndex } = invitedSmith;
      const identity = await this.getIdtyByIndexOrFail(ctx, idtyIndex);
      identity.smithStatus = SmithStatus.Invited;

      this.data.identities.set(idtyIndex, identity);
    }

    // Process Smith invitation accepted
    for (const acceptedSmithInvitations of newData.smithAccepted) {
      const { idtyIndex } = acceptedSmithInvitations;

      const identity = await this.getIdtyByIndexOrFail(ctx, idtyIndex);
      identity.smithStatus = SmithStatus.Pending;
      this.data.identities.set(idtyIndex, identity);
    }

    // Process Smith promotion
    for (const promotedSmith of newData.smithPromoted) {
      const { idtyIndex } = promotedSmith;

      const identity = await this.getIdtyByIndexOrFail(ctx, idtyIndex);
      identity.smithStatus = SmithStatus.Smith;
      this.data.identities.set(idtyIndex, identity);
    }

    // Process Smith exlusion
    for (const excludedSmith of newData.smithExcluded) {
      const { idtyIndex } = excludedSmith;

      const identity = await this.getIdtyByIndexOrFail(ctx, idtyIndex);

      identity.smithStatus = SmithStatus.Excluded;
      this.data.identities.set(idtyIndex, identity);
    }

    // Process account links
    for (const link of newData.accountLink) {
      // we can link an identity to a non-existing account
      const account = await this.getOrCreateAccount(ctx, link.accountId);
      // should never fail because identity must exist to be able to link itself
      const idty = await this.getIdtyByIndexOrFail(ctx, link.index);
      account.linkedIdentity = idty;
      this.data.accounts.set(account.id, account);
    }

    // Process account unlinks
    for (const unlink of newData.accountUnlink) {
      // should never fail because account must exist to unlink due to tx fees
      const account = await this.getAccountByAddressOrFail(ctx, unlink.accountId);
      account.linkedIdentity = null;
      this.data.accounts.set(account.id, account);
    }
  }

  // this is a hack to handle circular dependency
  async handleNewAccountsApart(ctx: Ctx, newData: NewData) {
    // Combine account and account link sets to get unique candidates
    const newAccountCandidates = new Set<Address>([
      ...newData.accounts,
      ...newData.accountLink.map((link) => link.accountId),
    ]);

    // Return immediately if no new candidates are present
    if (newAccountCandidates.size === 0) {
      return;
    }

    // Retrieve existing account IDs from the database
    const existingAccounts = await ctx.store.findBy(Account, {
      id: In([...newAccountCandidates]),
    });
    const existingAccountIds = new Set(
      existingAccounts.map((account) => account.id)
    );

    // Filter and create accounts that don't already exist
    const accountsToCreate = [...newAccountCandidates]
      .filter((id) => !existingAccountIds.has(id))
      .map((id) => new Account({ id }));

    if (accountsToCreate.length > 0) {
      await ctx.store.insert(accountsToCreate);
      await ctx.store.commit();
    }

    // Update the Data structure with new accounts
    for (const account of accountsToCreate) {
      const existingData = await this.getAccountByAddressOrFail(ctx, account.id);
      if (existingData) {
        account.linkedIdentity = existingData.linkedIdentity;
      }
      this.data.accounts.set(account.id, account);
    }
  }

  // this is a hack to handle circular dependency
  async handleNewIdentitiesApart(ctx: Ctx, newData: NewData) {
    const identities: Array<Identity> = [];
    for (const i of newData.identitiesCreated) {
      const idty = this.data.identities.get(i.index);
      assert(
        idty != null,
        "created identities must appear in prepared identities"
      );
      this.data.identities.delete(i.index); // prevent from trying to add twice
      identities.push(idty);
    }
    if (identities.length == 0) {
      return;
    }
    // we are sure that all created identities actually do not already exist in database
    await ctx.store.insert(identities);
    await ctx.store.commit();
  }

  /// store prepared data into database
  async storeData(ctx: Ctx) {
    // UPSERT = update or insert if not existing
    // account can have already existed, been killed, and recreated
    await ctx.store.upsert([...this.data.accounts.values()]);
    // identities can have been changed (confirmed, change owner key...) or added (created)
    await ctx.store.upsert([...this.data.identities.values()]);
    // membership can have been created, renewed, or removed
    await ctx.store.upsert([...this.data.membershipEvents.values()]);
    // certs can have been created, renewed, or removed
    await ctx.store.upsert([...this.data.certification.values()]);
    await ctx.store.upsert([...this.data.smithCert.values()]);

    // INSERT = these object can not exist before
    await ctx.store.insert(this.data.transfers);
    await ctx.store.insert(this.data.changeOwnerKey);
    await ctx.store.insert(this.data.certEvent);

    // Apply changes in database
    await ctx.store.commit();
  }

  async getOrCreateAccount(ctx: Ctx, id: Address): Promise<Account> {
    let account =
      this.data.accounts.get(id) ?? (await ctx.store.get(Account, id));
    if (account == null) {
      // we need to create it
      account = new Account({ id });
      this.data.accounts.set(id, account);
    }
    return account;
  }

  async getAccountByAddressOrFail(ctx: Ctx, address: Address): Promise<Account> {
    return (
      this.data.accounts.get(address) ??
      ctx.store.findOneByOrFail(Account, { id: address })
    );
  }

  async getIdtyByIndexOrFail(ctx: Ctx, index: IdtyIndex): Promise<Identity> {
    return (
      this.data.identities.get(index) ??
      ctx.store.findOneByOrFail(Identity, { index })
    );
  }
}

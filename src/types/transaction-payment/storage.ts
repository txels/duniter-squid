import {sts, Block, Bytes, Option, Result, StorageType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const nextFeeMultiplier =  {
    v800: new StorageType('TransactionPayment.NextFeeMultiplier', 'Default', [], v800.FixedU128) as NextFeeMultiplierV800,
}

export interface NextFeeMultiplierV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.FixedU128
    get(block: Block): Promise<(v800.FixedU128 | undefined)>
}

export const storageVersion =  {
    v800: new StorageType('TransactionPayment.StorageVersion', 'Default', [], v800.Releases) as StorageVersionV800,
}

export interface StorageVersionV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.Releases
    get(block: Block): Promise<(v800.Releases | undefined)>
}

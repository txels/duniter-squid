import {sts, Block, Bytes, Option, Result, ConstantType, RuntimeCtx} from '../support'

export const certPeriod =  {
    /**
     *  Minimum duration between two certifications issued by the same issuer.
     */
    v800: new ConstantType(
        'Certification.CertPeriod',
        sts.number()
    ),
}

export const maxByIssuer =  {
    /**
     *  Maximum number of active certifications by issuer.
     */
    v800: new ConstantType(
        'Certification.MaxByIssuer',
        sts.number()
    ),
}

export const minReceivedCertToBeAbleToIssueCert =  {
    /**
     *  Minimum number of certifications received to be allowed to issue a certification.
     */
    v800: new ConstantType(
        'Certification.MinReceivedCertToBeAbleToIssueCert',
        sts.number()
    ),
}

export const validityPeriod =  {
    /**
     *  Duration of validity of a certification.
     */
    v800: new ConstantType(
        'Certification.ValidityPeriod',
        sts.number()
    ),
}

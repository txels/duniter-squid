import {sts, Block, Bytes, Option, Result, EventType, RuntimeCtx} from '../support'

export const certAdded =  {
    name: 'Certification.CertAdded',
    /**
     * A new certification was added.
     */
    v800: new EventType(
        'Certification.CertAdded',
        sts.struct({
            issuer: sts.number(),
            receiver: sts.number(),
        })
    ),
}

export const certRemoved =  {
    name: 'Certification.CertRemoved',
    /**
     * A certification was removed.
     */
    v800: new EventType(
        'Certification.CertRemoved',
        sts.struct({
            issuer: sts.number(),
            receiver: sts.number(),
            expiration: sts.boolean(),
        })
    ),
}

export const certRenewed =  {
    name: 'Certification.CertRenewed',
    /**
     * A certification was renewed.
     */
    v800: new EventType(
        'Certification.CertRenewed',
        sts.struct({
            issuer: sts.number(),
            receiver: sts.number(),
        })
    ),
}

import {sts, Block, Bytes, Option, Result, CallType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const setKeys =  {
    name: 'Session.set_keys',
    /**
     * See [`Pallet::set_keys`].
     */
    v800: new CallType(
        'Session.set_keys',
        sts.struct({
            keys: v800.SessionKeys,
            proof: sts.bytes(),
        })
    ),
}

export const purgeKeys =  {
    name: 'Session.purge_keys',
    /**
     * See [`Pallet::purge_keys`].
     */
    v800: new CallType(
        'Session.purge_keys',
        sts.unit()
    ),
}

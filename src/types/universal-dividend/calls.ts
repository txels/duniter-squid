import {sts, Block, Bytes, Option, Result, CallType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const claimUds =  {
    name: 'UniversalDividend.claim_uds',
    /**
     * See [`Pallet::claim_uds`].
     */
    v800: new CallType(
        'UniversalDividend.claim_uds',
        sts.unit()
    ),
}

export const transferUd =  {
    name: 'UniversalDividend.transfer_ud',
    /**
     * See [`Pallet::transfer_ud`].
     */
    v800: new CallType(
        'UniversalDividend.transfer_ud',
        sts.struct({
            dest: v800.MultiAddress,
            value: sts.bigint(),
        })
    ),
}

export const transferUdKeepAlive =  {
    name: 'UniversalDividend.transfer_ud_keep_alive',
    /**
     * See [`Pallet::transfer_ud_keep_alive`].
     */
    v800: new CallType(
        'UniversalDividend.transfer_ud_keep_alive',
        sts.struct({
            dest: v800.MultiAddress,
            value: sts.bigint(),
        })
    ),
}

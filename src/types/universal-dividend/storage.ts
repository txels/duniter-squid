import {sts, Block, Bytes, Option, Result, StorageType, RuntimeCtx} from '../support'

export const currentUd =  {
    /**
     *  Current UD amount
     */
    v800: new StorageType('UniversalDividend.CurrentUd', 'Default', [], sts.bigint()) as CurrentUdV800,
}

/**
 *  Current UD amount
 */
export interface CurrentUdV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): bigint
    get(block: Block): Promise<(bigint | undefined)>
}

export const currentUdIndex =  {
    /**
     *  Current UD index
     */
    v800: new StorageType('UniversalDividend.CurrentUdIndex', 'Default', [], sts.number()) as CurrentUdIndexV800,
}

/**
 *  Current UD index
 */
export interface CurrentUdIndexV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number
    get(block: Block): Promise<(number | undefined)>
}

export const monetaryMass =  {
    /**
     *  Total quantity of money created by universal dividend (does not take into account the possible destruction of money)
     */
    v800: new StorageType('UniversalDividend.MonetaryMass', 'Default', [], sts.bigint()) as MonetaryMassV800,
}

/**
 *  Total quantity of money created by universal dividend (does not take into account the possible destruction of money)
 */
export interface MonetaryMassV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): bigint
    get(block: Block): Promise<(bigint | undefined)>
}

export const nextReeval =  {
    /**
     *  Next UD reevaluation
     */
    v800: new StorageType('UniversalDividend.NextReeval', 'Optional', [], sts.bigint()) as NextReevalV800,
}

/**
 *  Next UD reevaluation
 */
export interface NextReevalV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block): Promise<(bigint | undefined)>
}

export const nextUd =  {
    /**
     *  Next UD creation
     */
    v800: new StorageType('UniversalDividend.NextUd', 'Optional', [], sts.bigint()) as NextUdV800,
}

/**
 *  Next UD creation
 */
export interface NextUdV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block): Promise<(bigint | undefined)>
}

export const pastReevals =  {
    /**
     *  Past UD reevaluations
     */
    v800: new StorageType('UniversalDividend.PastReevals', 'Default', [], sts.array(() => sts.tuple(() => [sts.number(), sts.bigint()]))) as PastReevalsV800,
}

/**
 *  Past UD reevaluations
 */
export interface PastReevalsV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): [number, bigint][]
    get(block: Block): Promise<([number, bigint][] | undefined)>
}

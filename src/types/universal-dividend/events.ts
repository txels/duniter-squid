import {sts, Block, Bytes, Option, Result, EventType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const newUdCreated =  {
    name: 'UniversalDividend.NewUdCreated',
    /**
     * A new universal dividend is created.
     */
    v800: new EventType(
        'UniversalDividend.NewUdCreated',
        sts.struct({
            amount: sts.bigint(),
            index: sts.number(),
            monetaryMass: sts.bigint(),
            membersCount: sts.bigint(),
        })
    ),
}

export const udReevalued =  {
    name: 'UniversalDividend.UdReevalued',
    /**
     * The universal dividend has been re-evaluated.
     */
    v800: new EventType(
        'UniversalDividend.UdReevalued',
        sts.struct({
            newUdAmount: sts.bigint(),
            monetaryMass: sts.bigint(),
            membersCount: sts.bigint(),
        })
    ),
}

export const udsAutoPaid =  {
    name: 'UniversalDividend.UdsAutoPaid',
    /**
     * DUs were automatically transferred as part of a member removal.
     */
    v800: new EventType(
        'UniversalDividend.UdsAutoPaid',
        sts.struct({
            count: sts.number(),
            total: sts.bigint(),
            who: v800.AccountId32,
        })
    ),
}

export const udsClaimed =  {
    name: 'UniversalDividend.UdsClaimed',
    /**
     * A member claimed his UDs.
     */
    v800: new EventType(
        'UniversalDividend.UdsClaimed',
        sts.struct({
            count: sts.number(),
            total: sts.bigint(),
            who: v800.AccountId32,
        })
    ),
}

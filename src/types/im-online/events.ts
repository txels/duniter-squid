import {sts, Block, Bytes, Option, Result, EventType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const heartbeatReceived =  {
    name: 'ImOnline.HeartbeatReceived',
    /**
     * A new heartbeat was received from `AuthorityId`.
     */
    v800: new EventType(
        'ImOnline.HeartbeatReceived',
        sts.struct({
            authorityId: sts.bytes(),
        })
    ),
}

export const allGood =  {
    name: 'ImOnline.AllGood',
    /**
     * At the end of the session, no offence was committed.
     */
    v800: new EventType(
        'ImOnline.AllGood',
        sts.unit()
    ),
}

export const someOffline =  {
    name: 'ImOnline.SomeOffline',
    /**
     * At the end of the session, at least one validator was found to be offline.
     */
    v800: new EventType(
        'ImOnline.SomeOffline',
        sts.struct({
            offline: sts.array(() => sts.tuple(() => [v800.AccountId32, v800.ValidatorFullIdentification])),
        })
    ),
}

import {sts, Block, Bytes, Option, Result, CallType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const heartbeat =  {
    name: 'ImOnline.heartbeat',
    /**
     * See [`Pallet::heartbeat`].
     */
    v800: new CallType(
        'ImOnline.heartbeat',
        sts.struct({
            heartbeat: v800.Heartbeat,
            signature: sts.bytes(),
        })
    ),
}

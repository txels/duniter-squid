import {sts, Block, Bytes, Option, Result, CallType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const createIdentity =  {
    name: 'Identity.create_identity',
    /**
     * See [`Pallet::create_identity`].
     */
    v800: new CallType(
        'Identity.create_identity',
        sts.struct({
            ownerKey: v800.AccountId32,
        })
    ),
}

export const confirmIdentity =  {
    name: 'Identity.confirm_identity',
    /**
     * See [`Pallet::confirm_identity`].
     */
    v800: new CallType(
        'Identity.confirm_identity',
        sts.struct({
            idtyName: v800.IdtyName,
        })
    ),
}

export const changeOwnerKey =  {
    name: 'Identity.change_owner_key',
    /**
     * See [`Pallet::change_owner_key`].
     */
    v800: new CallType(
        'Identity.change_owner_key',
        sts.struct({
            newKey: v800.AccountId32,
            newKeySig: v800.MultiSignature,
        })
    ),
}

export const revokeIdentity =  {
    name: 'Identity.revoke_identity',
    /**
     * See [`Pallet::revoke_identity`].
     */
    v800: new CallType(
        'Identity.revoke_identity',
        sts.struct({
            idtyIndex: sts.number(),
            revocationKey: v800.AccountId32,
            revocationSig: v800.MultiSignature,
        })
    ),
}

export const pruneItemIdentitiesNames =  {
    name: 'Identity.prune_item_identities_names',
    /**
     * See [`Pallet::prune_item_identities_names`].
     */
    v800: new CallType(
        'Identity.prune_item_identities_names',
        sts.struct({
            names: sts.array(() => v800.IdtyName),
        })
    ),
}

export const fixSufficients =  {
    name: 'Identity.fix_sufficients',
    /**
     * See [`Pallet::fix_sufficients`].
     */
    v800: new CallType(
        'Identity.fix_sufficients',
        sts.struct({
            ownerKey: v800.AccountId32,
            inc: sts.boolean(),
        })
    ),
}

export const linkAccount =  {
    name: 'Identity.link_account',
    /**
     * See [`Pallet::link_account`].
     */
    v800: new CallType(
        'Identity.link_account',
        sts.struct({
            accountId: v800.AccountId32,
            payloadSig: v800.MultiSignature,
        })
    ),
}

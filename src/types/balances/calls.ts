import {sts, Block, Bytes, Option, Result, CallType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const transferAllowDeath =  {
    name: 'Balances.transfer_allow_death',
    /**
     * See [`Pallet::transfer_allow_death`].
     */
    v800: new CallType(
        'Balances.transfer_allow_death',
        sts.struct({
            dest: v800.MultiAddress,
            value: sts.bigint(),
        })
    ),
}

export const forceTransfer =  {
    name: 'Balances.force_transfer',
    /**
     * See [`Pallet::force_transfer`].
     */
    v800: new CallType(
        'Balances.force_transfer',
        sts.struct({
            source: v800.MultiAddress,
            dest: v800.MultiAddress,
            value: sts.bigint(),
        })
    ),
}

export const transferKeepAlive =  {
    name: 'Balances.transfer_keep_alive',
    /**
     * See [`Pallet::transfer_keep_alive`].
     */
    v800: new CallType(
        'Balances.transfer_keep_alive',
        sts.struct({
            dest: v800.MultiAddress,
            value: sts.bigint(),
        })
    ),
}

export const transferAll =  {
    name: 'Balances.transfer_all',
    /**
     * See [`Pallet::transfer_all`].
     */
    v800: new CallType(
        'Balances.transfer_all',
        sts.struct({
            dest: v800.MultiAddress,
            keepAlive: sts.boolean(),
        })
    ),
}

export const forceUnreserve =  {
    name: 'Balances.force_unreserve',
    /**
     * See [`Pallet::force_unreserve`].
     */
    v800: new CallType(
        'Balances.force_unreserve',
        sts.struct({
            who: v800.MultiAddress,
            amount: sts.bigint(),
        })
    ),
}

export const forceSetBalance =  {
    name: 'Balances.force_set_balance',
    /**
     * See [`Pallet::force_set_balance`].
     */
    v800: new CallType(
        'Balances.force_set_balance',
        sts.struct({
            who: v800.MultiAddress,
            newFree: sts.bigint(),
        })
    ),
}

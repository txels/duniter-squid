import {sts, Block, Bytes, Option, Result, StorageType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const totalIssuance =  {
    /**
     *  The total units issued in the system.
     */
    v800: new StorageType('Balances.TotalIssuance', 'Default', [], sts.bigint()) as TotalIssuanceV800,
}

/**
 *  The total units issued in the system.
 */
export interface TotalIssuanceV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): bigint
    get(block: Block): Promise<(bigint | undefined)>
}

export const inactiveIssuance =  {
    /**
     *  The total units of outstanding deactivated balance in the system.
     */
    v800: new StorageType('Balances.InactiveIssuance', 'Default', [], sts.bigint()) as InactiveIssuanceV800,
}

/**
 *  The total units of outstanding deactivated balance in the system.
 */
export interface InactiveIssuanceV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): bigint
    get(block: Block): Promise<(bigint | undefined)>
}

export const account =  {
    /**
     *  The Balances pallet example of storing the balance of an account.
     * 
     *  # Example
     * 
     *  ```nocompile
     *   impl pallet_balances::Config for Runtime {
     *     type AccountStore = StorageMapShim<Self::Account<Runtime>, frame_system::Provider<Runtime>, AccountId, Self::AccountData<Balance>>
     *   }
     *  ```
     * 
     *  You can also store the balance of an account in the `System` pallet.
     * 
     *  # Example
     * 
     *  ```nocompile
     *   impl pallet_balances::Config for Runtime {
     *    type AccountStore = System
     *   }
     *  ```
     * 
     *  But this comes with tradeoffs, storing account balances in the system pallet stores
     *  `frame_system` data alongside the account data contrary to storing account balances in the
     *  `Balances` pallet, which uses a `StorageMap` to store balances data only.
     *  NOTE: This is only used in the case that this pallet is used to store balances.
     */
    v800: new StorageType('Balances.Account', 'Default', [v800.AccountId32], v800.Type_205) as AccountV800,
}

/**
 *  The Balances pallet example of storing the balance of an account.
 * 
 *  # Example
 * 
 *  ```nocompile
 *   impl pallet_balances::Config for Runtime {
 *     type AccountStore = StorageMapShim<Self::Account<Runtime>, frame_system::Provider<Runtime>, AccountId, Self::AccountData<Balance>>
 *   }
 *  ```
 * 
 *  You can also store the balance of an account in the `System` pallet.
 * 
 *  # Example
 * 
 *  ```nocompile
 *   impl pallet_balances::Config for Runtime {
 *    type AccountStore = System
 *   }
 *  ```
 * 
 *  But this comes with tradeoffs, storing account balances in the system pallet stores
 *  `frame_system` data alongside the account data contrary to storing account balances in the
 *  `Balances` pallet, which uses a `StorageMap` to store balances data only.
 *  NOTE: This is only used in the case that this pallet is used to store balances.
 */
export interface AccountV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.Type_205
    get(block: Block, key: v800.AccountId32): Promise<(v800.Type_205 | undefined)>
    getMany(block: Block, keys: v800.AccountId32[]): Promise<(v800.Type_205 | undefined)[]>
    getKeys(block: Block): Promise<v800.AccountId32[]>
    getKeys(block: Block, key: v800.AccountId32): Promise<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<v800.AccountId32[]>
    getPairs(block: Block): Promise<[k: v800.AccountId32, v: (v800.Type_205 | undefined)][]>
    getPairs(block: Block, key: v800.AccountId32): Promise<[k: v800.AccountId32, v: (v800.Type_205 | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: v800.AccountId32, v: (v800.Type_205 | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<[k: v800.AccountId32, v: (v800.Type_205 | undefined)][]>
}

export const locks =  {
    /**
     *  Any liquidity locks on some account balances.
     *  NOTE: Should only be accessed when setting, changing and freeing a lock.
     */
    v800: new StorageType('Balances.Locks', 'Default', [v800.AccountId32], sts.array(() => v800.BalanceLock)) as LocksV800,
}

/**
 *  Any liquidity locks on some account balances.
 *  NOTE: Should only be accessed when setting, changing and freeing a lock.
 */
export interface LocksV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.BalanceLock[]
    get(block: Block, key: v800.AccountId32): Promise<(v800.BalanceLock[] | undefined)>
    getMany(block: Block, keys: v800.AccountId32[]): Promise<(v800.BalanceLock[] | undefined)[]>
    getKeys(block: Block): Promise<v800.AccountId32[]>
    getKeys(block: Block, key: v800.AccountId32): Promise<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<v800.AccountId32[]>
    getPairs(block: Block): Promise<[k: v800.AccountId32, v: (v800.BalanceLock[] | undefined)][]>
    getPairs(block: Block, key: v800.AccountId32): Promise<[k: v800.AccountId32, v: (v800.BalanceLock[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: v800.AccountId32, v: (v800.BalanceLock[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<[k: v800.AccountId32, v: (v800.BalanceLock[] | undefined)][]>
}

export const reserves =  {
    /**
     *  Named reserves on some account balances.
     */
    v800: new StorageType('Balances.Reserves', 'Default', [v800.AccountId32], sts.array(() => v800.ReserveData)) as ReservesV800,
}

/**
 *  Named reserves on some account balances.
 */
export interface ReservesV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.ReserveData[]
    get(block: Block, key: v800.AccountId32): Promise<(v800.ReserveData[] | undefined)>
    getMany(block: Block, keys: v800.AccountId32[]): Promise<(v800.ReserveData[] | undefined)[]>
    getKeys(block: Block): Promise<v800.AccountId32[]>
    getKeys(block: Block, key: v800.AccountId32): Promise<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<v800.AccountId32[]>
    getPairs(block: Block): Promise<[k: v800.AccountId32, v: (v800.ReserveData[] | undefined)][]>
    getPairs(block: Block, key: v800.AccountId32): Promise<[k: v800.AccountId32, v: (v800.ReserveData[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: v800.AccountId32, v: (v800.ReserveData[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<[k: v800.AccountId32, v: (v800.ReserveData[] | undefined)][]>
}

export const holds =  {
    /**
     *  Holds on account balances.
     */
    v800: new StorageType('Balances.Holds', 'Default', [v800.AccountId32], sts.array(() => v800.IdAmount)) as HoldsV800,
}

/**
 *  Holds on account balances.
 */
export interface HoldsV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.IdAmount[]
    get(block: Block, key: v800.AccountId32): Promise<(v800.IdAmount[] | undefined)>
    getMany(block: Block, keys: v800.AccountId32[]): Promise<(v800.IdAmount[] | undefined)[]>
    getKeys(block: Block): Promise<v800.AccountId32[]>
    getKeys(block: Block, key: v800.AccountId32): Promise<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<v800.AccountId32[]>
    getPairs(block: Block): Promise<[k: v800.AccountId32, v: (v800.IdAmount[] | undefined)][]>
    getPairs(block: Block, key: v800.AccountId32): Promise<[k: v800.AccountId32, v: (v800.IdAmount[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: v800.AccountId32, v: (v800.IdAmount[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<[k: v800.AccountId32, v: (v800.IdAmount[] | undefined)][]>
}

export const freezes =  {
    /**
     *  Freeze locks on account balances.
     */
    v800: new StorageType('Balances.Freezes', 'Default', [v800.AccountId32], sts.array(() => v800.IdAmount)) as FreezesV800,
}

/**
 *  Freeze locks on account balances.
 */
export interface FreezesV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.IdAmount[]
    get(block: Block, key: v800.AccountId32): Promise<(v800.IdAmount[] | undefined)>
    getMany(block: Block, keys: v800.AccountId32[]): Promise<(v800.IdAmount[] | undefined)[]>
    getKeys(block: Block): Promise<v800.AccountId32[]>
    getKeys(block: Block, key: v800.AccountId32): Promise<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<v800.AccountId32[]>
    getPairs(block: Block): Promise<[k: v800.AccountId32, v: (v800.IdAmount[] | undefined)][]>
    getPairs(block: Block, key: v800.AccountId32): Promise<[k: v800.AccountId32, v: (v800.IdAmount[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: v800.AccountId32, v: (v800.IdAmount[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<[k: v800.AccountId32, v: (v800.IdAmount[] | undefined)][]>
}

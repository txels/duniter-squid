import {sts, Block, Bytes, Option, Result, EventType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const extrinsicSuccess =  {
    name: 'System.ExtrinsicSuccess',
    /**
     * An extrinsic completed successfully.
     */
    v800: new EventType(
        'System.ExtrinsicSuccess',
        sts.struct({
            dispatchInfo: v800.DispatchInfo,
        })
    ),
}

export const extrinsicFailed =  {
    name: 'System.ExtrinsicFailed',
    /**
     * An extrinsic failed.
     */
    v800: new EventType(
        'System.ExtrinsicFailed',
        sts.struct({
            dispatchError: v800.DispatchError,
            dispatchInfo: v800.DispatchInfo,
        })
    ),
}

export const codeUpdated =  {
    name: 'System.CodeUpdated',
    /**
     * `:code` was updated.
     */
    v800: new EventType(
        'System.CodeUpdated',
        sts.unit()
    ),
}

export const newAccount =  {
    name: 'System.NewAccount',
    /**
     * A new account was created.
     */
    v800: new EventType(
        'System.NewAccount',
        sts.struct({
            account: v800.AccountId32,
        })
    ),
}

export const killedAccount =  {
    name: 'System.KilledAccount',
    /**
     * An account was reaped.
     */
    v800: new EventType(
        'System.KilledAccount',
        sts.struct({
            account: v800.AccountId32,
        })
    ),
}

export const remarked =  {
    name: 'System.Remarked',
    /**
     * On on-chain remark happened.
     */
    v800: new EventType(
        'System.Remarked',
        sts.struct({
            sender: v800.AccountId32,
            hash: v800.H256,
        })
    ),
}

export const upgradeAuthorized =  {
    name: 'System.UpgradeAuthorized',
    /**
     * An upgrade was authorized.
     */
    v800: new EventType(
        'System.UpgradeAuthorized',
        sts.struct({
            codeHash: v800.H256,
            checkVersion: sts.boolean(),
        })
    ),
}

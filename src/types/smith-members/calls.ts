import {sts, Block, Bytes, Option, Result, CallType, RuntimeCtx} from '../support'

export const inviteSmith =  {
    name: 'SmithMembers.invite_smith',
    /**
     * See [`Pallet::invite_smith`].
     */
    v800: new CallType(
        'SmithMembers.invite_smith',
        sts.struct({
            receiver: sts.number(),
        })
    ),
}

export const acceptInvitation =  {
    name: 'SmithMembers.accept_invitation',
    /**
     * See [`Pallet::accept_invitation`].
     */
    v800: new CallType(
        'SmithMembers.accept_invitation',
        sts.unit()
    ),
}

export const certifySmith =  {
    name: 'SmithMembers.certify_smith',
    /**
     * See [`Pallet::certify_smith`].
     */
    v800: new CallType(
        'SmithMembers.certify_smith',
        sts.struct({
            receiver: sts.number(),
        })
    ),
}

import {sts, Block, Bytes, Option, Result, ConstantType, RuntimeCtx} from '../support'

export const membershipPeriod =  {
    /**
     *  Maximum life span of a single membership (in number of blocks)
     */
    v800: new ConstantType(
        'Membership.MembershipPeriod',
        sts.number()
    ),
}

export const membershipRenewalPeriod =  {
    /**
     *  Minimum delay to wait before renewing membership
     */
    v800: new ConstantType(
        'Membership.MembershipRenewalPeriod',
        sts.number()
    ),
}

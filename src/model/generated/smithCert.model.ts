import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, ManyToOne as ManyToOne_, Index as Index_} from "typeorm"
import {Identity} from "./identity.model"

/**
 * Smith certification
 */
@Entity_()
export class SmithCert {
    constructor(props?: Partial<SmithCert>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    @Index_()
    @ManyToOne_(() => Identity, {nullable: true})
    issuer!: Identity

    @Index_()
    @ManyToOne_(() => Identity, {nullable: true})
    receiver!: Identity

    @Column_("int4", {nullable: false})
    createdOn!: number
}

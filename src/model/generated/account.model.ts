import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, OneToMany as OneToMany_, ManyToOne as ManyToOne_, Index as Index_} from "typeorm"
import {Transfer} from "./transfer.model"
import {Identity} from "./identity.model"
import {ChangeOwnerKey} from "./changeOwnerKey.model"

@Entity_()
export class Account {
    constructor(props?: Partial<Account>) {
        Object.assign(this, props)
    }

    /**
     * Account address is SS58 format
     */
    @PrimaryColumn_()
    id!: string

    @OneToMany_(() => Transfer, e => e.from)
    transfersIssued!: Transfer[]

    @OneToMany_(() => Transfer, e => e.to)
    transfersReceived!: Transfer[]

    /**
     * current account for the identity
     */

    /**
     * was once account of the identity
     */
    @OneToMany_(() => ChangeOwnerKey, e => e.previous)
    wasIdentity!: ChangeOwnerKey[]

    /**
     * linked to the identity
     */
    @Index_()
    @ManyToOne_(() => Identity, {nullable: true})
    linkedIdentity!: Identity | undefined | null
}

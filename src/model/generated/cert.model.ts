import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, ManyToOne as ManyToOne_, Index as Index_, OneToMany as OneToMany_} from "typeorm"
import {Identity} from "./identity.model"
import {CertEvent} from "./certEvent.model"

/**
 * Certification
 */
@Entity_()
export class Cert {
    constructor(props?: Partial<Cert>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    @Index_()
    @ManyToOne_(() => Identity, {nullable: true})
    issuer!: Identity

    @Index_()
    @ManyToOne_(() => Identity, {nullable: true})
    receiver!: Identity

    /**
     * whether the certification is currently active or not
     */
    @Column_("bool", {nullable: false})
    isActive!: boolean

    /**
     * the last createdOn value
     */
    @Column_("int4", {nullable: false})
    createdOn!: number

    /**
     * the current expireOn value
     */
    @Column_("int4", {nullable: false})
    expireOn!: number

    @OneToMany_(() => CertEvent, e => e.cert)
    certHistory!: CertEvent[]
}

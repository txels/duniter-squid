export enum EventType {
    CREATION = "CREATION",
    RENEWAL = "RENEWAL",
    REMOVAL = "REMOVAL",
}

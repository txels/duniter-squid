import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, ManyToOne as ManyToOne_, Index as Index_} from "typeorm"
import {Identity} from "./identity.model"
import {EventType} from "./_eventType"
import {Event} from "./event.model"

@Entity_()
export class MembershipEvent {
    constructor(props?: Partial<MembershipEvent>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    @Index_()
    @ManyToOne_(() => Identity, {nullable: true})
    identity!: Identity

    @Column_("varchar", {length: 8, nullable: false})
    eventType!: EventType

    @Index_()
    @ManyToOne_(() => Event, {nullable: true})
    event!: Event

    @Index_()
    @Column_("int4", {nullable: false})
    blockNumber!: number
}

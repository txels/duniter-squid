import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, Index as Index_, OneToOne as OneToOne_, JoinColumn as JoinColumn_, ManyToOne as ManyToOne_, OneToMany as OneToMany_} from "typeorm"
import {Account} from "./account.model"
import {IdentityStatus} from "./_identityStatus"
import {Event} from "./event.model"
import {SmithStatus} from "./_smithStatus"
import {Cert} from "./cert.model"
import {SmithCert} from "./smithCert.model"
import {MembershipEvent} from "./membershipEvent.model"
import {ChangeOwnerKey} from "./changeOwnerKey.model"

/**
 * Identity
 */
@Entity_()
export class Identity {
    constructor(props?: Partial<Identity>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    /**
     * Identity index
     */
    @Index_({unique: true})
    @Column_("int4", {nullable: false})
    index!: number

    /**
     * Current account
     */
    @Index_({unique: true})
    @OneToOne_(() => Account, {nullable: true})
    @JoinColumn_()
    account!: Account

    /**
     * Name
     */
    @Index_()
    @Column_("text", {nullable: false})
    name!: string

    /**
     * Status of the identity
     */
    @Index_()
    @Column_("varchar", {length: 11, nullable: false})
    status!: IdentityStatus

    /**
     * Block number of identity creation event
     */
    @Column_("int4", {nullable: false})
    createdOn!: number

    /**
     * Event corresponding of identity creation event
     */
    @Index_()
    @ManyToOne_(() => Event, {nullable: true})
    createdIn!: Event

    /**
     * Block number of last identity, changeOwnerKey and membership event
     */
    @Column_("int4", {nullable: false})
    lastChangeOn!: number

    /**
     * Smith status of the identity
     */
    @Column_("varchar", {length: 8, nullable: true})
    smithStatus!: SmithStatus | undefined | null

    /**
     * Certifications issued
     */
    @OneToMany_(() => Cert, e => e.issuer)
    certIssued!: Cert[]

    /**
     * Certifications received
     */
    @OneToMany_(() => Cert, e => e.receiver)
    certReceived!: Cert[]

    /**
     * Smith certifications issued
     */
    @OneToMany_(() => SmithCert, e => e.issuer)
    smithCertIssued!: SmithCert[]

    /**
     * Smith certifications received
     */
    @OneToMany_(() => SmithCert, e => e.receiver)
    smithCertReceived!: SmithCert[]

    /**
     * True if the identity is a member
     */
    @Column_("bool", {nullable: false})
    isMember!: boolean

    /**
     * the current expireOn value
     */
    @Column_("int4", {nullable: false})
    expireOn!: number

    /**
     * history of the membership changes events
     */
    @OneToMany_(() => MembershipEvent, e => e.identity)
    membershipHistory!: MembershipEvent[]

    /**
     * Owner key changes
     */
    @OneToMany_(() => ChangeOwnerKey, e => e.identity)
    ownerKeyChange!: ChangeOwnerKey[]

    /**
     * linked accounts
     */
    @OneToMany_(() => Account, e => e.linkedIdentity)
    linkedAccount!: Account[]
}

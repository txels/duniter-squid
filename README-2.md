## Installation d'outils

```sh
pnpm install --global @subsquid/cli@latest
```

## Processus de mise-à-jour du runtime

Pour mettre à jour les métadonnées du runtime

```sh
pnpm install --global @subsquid/substrate-metadata-explorer
squid-substrate-metadata-explorer --rpc ws://127.0.0.1:9944 --out gdev-metadata.jsonl
```

## Processus de mise-à-jour du réseau

Pour mettre à jour les assets (nouveau réseau)

```sh
# copier les liens depuis https://git.duniter.org/nodes/rust/duniter-v2s/-/releases/
# 1. genesis
wget https://nodes.pages.duniter.org/-/rust/duniter-v2s/-/jobs/120948/artifacts/release/gdev.json -O ./assets/gdev.json
# 2. tx history
wget https://nodes.pages.duniter.org/-/rust/duniter-v2s/-/jobs/126142/artifacts/release/gdev-indexer.json -O ./assets/gdev-indexer.json
jq ".transactions_history" ./assets/gdev-indexer.json > ./assets/history.json
rm ./assets/gdev-indexer.json
```

Note : pour l'instant `gdev-indexer.json` compile tout dans un énorme fichier, mais ici on préfère séparer en plusieurs fichiers, d'où la ligne jq pour extraire "transaction_history". CI duniter à mettre à jour.

## Publication d'image docker

Ce que je fais pour publier l'image

```sh
docker build . -t h30x/duniter-squid
docker image push h30x/duniter-squid
```